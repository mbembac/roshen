#!/bin/bash

echo "Starting scripts...."

# system=$(uname -s)
# cmd=$(time (python 1_GenerateInputROIs.py); time (python 2_TrainPyModel.py); time (python 3_ParseAndEvaluateOutput.py); time (python 4_VisualizeOutputROIs.py))

nohup time (python 1_GenerateInputROIs.py) > 1.log &
echo ".....Finished 1_GenerateInputROIs"

nohup time (python 2_TrainPyModel.py) > 2.log &
echo ".....Finished 2_TrainPyModel"

nohup time (python 3_ParseAndEvaluateOutput.py) > 3.log &
echo ".....Finished 3_ParseAndEvaluateOutput"

nohup time (python 4_VisualizeOutputROIs.py) > 4.log &
echo ".....Finished 4_VisualizeOutputROIs"

nohup time (python 5_RoshenMetrics.py) > 5.log &
echo ".....Finished 5_RoshenMetrics"

echo ".....Finished scripts"

# if [ "$system" != "Darwin" ] && [ "$system" != "Linux" ]; then
#     cmd=$(bash time (python 1_GenerateInputROIs.py); time (python 2_TrainPyModel.py); time (python 3_ParseAndEvaluateOutput.py); time (python 4_VisualizeOutputROIs.py))
# fi

# $cmd

