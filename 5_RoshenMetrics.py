from __future__ import print_function
import os
import importlib
import sys
import PARAMETERS
import pandas as pd
from sklearn.metrics import classification_report, confusion_matrix
from cntk_helpers import imWidthHeight, nnPredict, applyNonMaximaSuppression
TRUE_POSITIVE, TRUE_NEGATIVE, FALSE_POSITIVE, FALSE_NEGATIVE, UNKNOWN = 4, 3, 2, 1, 0

####################################
# Parameters
####################################
p = PARAMETERS.get_parameters_for_dataset()
image_set = 'test'      # 'train', 'test'
cntkParsedOutputDir = os.path.join(p.cntkFilesDir, image_set + "_parsed/")
####################################
# gets a dictionary of true_labels, predicted labels, true_coords, predicted_coord
# if all classess identified in both, return TP


def get_classifier_labels(in_dict):
    classes_no_bg = set(p.classes) - set(['__background__'])
    nclasses = len(classes_no_bg)
    true_labels = set(in_dict["true_labels"])
    pred_labels = set(in_dict["predicted_labels"])
    y = (true_labels == classes_no_bg)
    y_pred = (pred_labels == classes_no_bg)
    return y, y_pred


def print_stats(in_arr):
    y = [i[0] for i in in_arr]
    y_pred = [i[1] for i in in_arr]
    print(classification_report(y, y_pred))
    print(confusion_matrix(y, y_pred))


####################################
# Main
####################################


# loop over all images and visualize
imdb = p.imdbs[image_set]
values = []
for imgIndex in range(0, imdb.num_images):
    # metadata
    imgPath = imdb.image_path_at(imgIndex)
    imgWidth, imgHeight = imWidthHeight(imgPath)
    scale = 800.0 / max(imgWidth, imgHeight)
    roiRelCoords = imdb.roidb[imgIndex]['boxes']

    # evaluate classifier for all rois
    labels, scores = nnPredict(
        imgIndex, cntkParsedOutputDir, p.cntk_nrRois, len(p.classes), None)

    # remove the zero-padded rois
    scores = scores[:len(roiRelCoords)]
    labels = labels[:len(roiRelCoords)]

    # remove background and low confidence roi
    nmsKeepIndices = applyNonMaximaSuppression(
        p.nmsThreshold, labels, scores, roiRelCoords)
    selected_ind = [i for i, l in enumerate(
        labels[:len(roiRelCoords)]) if i in nmsKeepIndices and l != 0]
    selected_coords = [rel_coords for i, rel_coords in enumerate(
        roiRelCoords) if i in selected_ind]

    values_dict = {}
    # get predicted truths
    values_dict["predicted_scores"] = scores[:len(
        imdb.roidb[imgIndex]['boxes'])]  # TBI filter nms and background
    values_dict["predicted_labels"] = [p.classes[l]
                                       for i, l in enumerate(labels[:len(roiRelCoords)]) if i in selected_ind]
    values_dict["predicted_boxes"] = [
        [int(scale * i) for i in coord] for coord in selected_coords]

    # get ground truths
    values_dict["true_labels"] = open(
        imgPath[:-4] + ".bboxes.labels.tsv", 'r').read().strip().split('\n')
    values_dict["true_boxes"] = open(
        imgPath[:-4] + ".bboxes.tsv", 'r').read().strip().split('\n')

    # stays here because Ari wrote it

    # values_dict["true_values"] = [[tl , tb.split('\t')] for tl, tb in zip(true_labels, true_boxes)]
    # values = [{"true_labels":tl , "true_boxes":tb.split('\t'), "predicted_label":pl,"predicted_boxes":pb} for
    # tl, tb, pl, pb in zip(true_labels, true_boxes, predicted_labels,
    # predicted_boxes)]

    values.append(values_dict)

    # calculate metrics

    # print(values)


print_stats([(get_classifier_labels(i)) for i in values])
print ("DONE.")
