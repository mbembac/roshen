# Roshen Hackfest

Using CNTK to do object detection on Roshen chocolate product placement

## Getting Started


### Prerequisites

* [CNTK installed](https://github.com/Microsoft/CNTK/wiki/Setup-CNTK-on-your-machine) - *ideally on a machine with NVIDIA GPUs*


### Setup

1. Create a folder exactly called `DataSets` in the root of this repo
    - There you will place your data sets

2. Run `python install_fastrcnn.py`
    - This will download a sample dataset to `DataSets` and the pre-trained `AlexNet` model
    - **ALEXNET MODEL IS ABSOLUTELY NEEDED**

3. Modify `PARAMETERS.py` to tweak your inputs and let the scripts know which dataset to use.

### Running

1. Run `python 1_GenerateInputROIs.py` to generate the input ROIs for training and testing.

2. Run `python 2_TrainPyModel.py` to train a Fast R-CNN model using the CNTK Python API and compute test results.

3. Or to train with multiple GPUs run:

	* [**MUST HAVE SYSTEM CONFIGURED WITH OPEMMPI**](https://github.com/Microsoft/CNTK/wiki/Multiple-GPUs-and-machines)

	```
mpiexec --npernode 2 -mca btl ^openib python 2_TrainWithMulitpleGPUs.py
```

4. Run `python 3_ParseAndEvaluateOutput.py` to compute the mAP (mean average precision) of the trained model.

5. To visualize the bounding boxes and predicted labels you can run `python 4_VisualizeOutputROIs.py`
    - Image results are saved to `results/{datasetName}/visualizations/`

6. To evaluate the model run `python 5_RoshenMetrics.py`


## Authors

* **Claudius Mbemba** - *Initial work* - [User1m](https://github.com/User1m)
* **Ari Bornstein** - *Initial work* - [aribornstein](https://github.com/aribornstein)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* [CNTK Object Detection using Fast RCNN](https://github.com/Microsoft/CNTK/wiki/Object-Detection-using-Fast-R-CNN)
